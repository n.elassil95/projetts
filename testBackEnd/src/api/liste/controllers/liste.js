'use strict';

/**
 * liste controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::liste.liste');
