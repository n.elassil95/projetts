'use strict';

/**
 * liste router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::liste.liste');
