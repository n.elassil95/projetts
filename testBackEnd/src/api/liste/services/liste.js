'use strict';

/**
 * liste service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::liste.liste');
