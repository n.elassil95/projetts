
import React, { useState } from 'react';
//import axios from 'axios';
export default function Todolist() {
  // const axios = require('axios');


  const url = "http://localhost:1337/api/listes";

  async function ajouter() {

    try {
      const response = await fetch(url,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            "data": {
              "titre": form.titre,
              "description": form.description

            }

          })
        });
      // console.log(form)
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const createdElement = await response.json();

      return createdElement;

      //return  alert("hi")
    } catch (error) {
      console.error(error);
    }
  }

  async function MoudifierListe(id:number) {

    try {
      const response = await fetch(url + `/` + id, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "data": {
            "fait": true

          }


        })
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const MoudifierListe = await response.json();

      // console.log(MoudifierListe.data)

      return MoudifierListe.data;
    } catch (error) {
      console.error(error);
    }
  }
  async function MoudifierListe2(id:number) {

    try {
      const response = await fetch(url + `/` + id, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "data": {
            "fait": false

          }


        })
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const MoudifierListe = await response.json();

      // console.log(MoudifierListe.data)

      return MoudifierListe.data;
    } catch (error) {
      console.error(error);
    }
  }
  async function supprimerDeListe(id:number) {//supprimer etudient
    console.log(id)
    //e.preventDefault();
    try {
      const response = await fetch(url + `/` + id, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json'
        },

      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const supprimer = await response.json();
      //console.log(suppEtudiant)
      return supprimer ;
    } catch (error) {
      console.error(error);
    }
  }



  async function fetchListe() {//afficher mon base de donnée
   
    const response = await fetch(url);
    const liste = await response.json();
    let tabElement = document.getElementById("tab");
    if (tabElement !== null) {
      tabElement.innerHTML = "";
    }
    //document.getElementById("tab").innerHTML ="";
    let table = document.createElement('table');
    let thead = document.createElement('thead');
    let tbody = document.createElement('tbody');
    table.className = "outline border border-slate-300 rounded-lg float-right ml-10 ";
    table.appendChild(thead);
    table.appendChild(tbody);
    
    if (tabElement !== null) {
      tabElement.appendChild(table);
    }
   //document.getElementById('tab').appendChild(table);
    let row_1 = document.createElement('tr');

    let heading_1 = document.createElement('th');
    heading_1.innerHTML = `titre`;
    heading_1.className = "border border-slate-300 w-26 "
    let heading_2 = document.createElement('th');
    heading_2.innerHTML = `Description`;
    heading_2.className = "border border-slate-300 w-26"
    let heading_3 = document.createElement('th');
    heading_3.innerHTML = `fait`;
    heading_3.className = "border border-slate-300 w-26 "
    let heading_4 = document.createElement('th');
    heading_4.innerHTML = `Action`;
    heading_4.className = "border border-slate-300 w-26 "


    row_1.appendChild(heading_1);
    row_1.appendChild(heading_2);
    row_1.appendChild(heading_3);
    row_1.appendChild(heading_4);
    thead.appendChild(row_1);
    for (let i = 0; i < liste.data.length; i++) {
      let row_2 = document.createElement('tr');
      let cellTitre = document.createElement('td');
      cellTitre.innerHTML = liste.data[i].attributes.titre;
      cellTitre.className = "border border-slate-300 w-40"
      let cellDescription = document.createElement('td');
      cellDescription.innerHTML = liste.data[i].attributes.description;
      cellDescription.className = "border border-slate-300 w-40"
      let checkbox = document.createElement('td');
      let cellFait = document.createElement('input'); // Créer un élément <input>
      cellFait.type = 'checkbox';
      cellFait.textContent = 'Fait';
      cellFait.id = liste.data[i].id;
      
      checkbox.className = "border border-slate-300 w-35"
      cellFait.onclick = () => MoudifierListe(liste.data[i].id);
      if (liste.data[i].attributes.fait === true) {
        row_2.style.textDecoration = 'line-through';
        cellFait.checked = true;
        cellFait.onclick = () => MoudifierListe2(liste.data[i].id);
        
      }
      let Action = document.createElement('td');
   
      const ActionSupprimer = document.createElement('BUTTON') as HTMLInputElement;
    if ("BUTTON" in ActionSupprimer) {
     ActionSupprimer.type = 'submit';
    }
      //ActionSupprimer.type = 'submit';
      ActionSupprimer.textContent = 'supprimer';
      ActionSupprimer.onclick = () => supprimerDeListe(liste.data[i].id);
      ActionSupprimer.className = "border border-slate-300 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 border border-blue-700 rounded w-26"
      row_2.appendChild(cellTitre);
      row_2.appendChild(cellDescription);
      row_2.appendChild(checkbox);
      row_2.appendChild(Action);
      checkbox.appendChild(cellFait)
      Action.appendChild(ActionSupprimer);
      tbody.appendChild(row_2);
    }
  }
  console.log('hi')
  fetchListe();


  const [form, setForm] = useState({
    titre: '', description: ''
  });
  const handleChangeTitre = (event:any) => {
    setForm({
      ...form,
      titre: event.target.value
    });
    //console.log(form);
  };
  const handleChangeDescription = (event:any) => {
    setForm({
      ...form,
      description: event.target.value
    });
  
  };
  return (
    <div className='static'>
      <form onSubmit={ajouter}>
      <h1 className="text-3xl text-black-700 font-bold mb-5">To do list</h1>
      <div className='float-left ' >
      <div >
        <label>Titre<em>*</em> :</label><br />
        <input className="square border border-dark" type="text" id="nom" name="nom" onChange={handleChangeTitre} />
      </div>
      <div>
        <label>Description<em>*</em> :</label><br />
        <textarea className="square border border-dark" name="Text1" cols= {40} rows={5} onChange={handleChangeDescription}></textarea>
      </div>
      <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 border border-green-700 rounded w-24"type="submit">Valider</button>

      </div >
      </form>
    </div>
  )
}
