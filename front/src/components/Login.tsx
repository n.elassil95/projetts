//import { useState } from "react";
import React ,{useState } from 'react';
//import Todolist from './todolist';
import { Link } from 'react-router-dom';
//import { LogingContext } from './LogingContext/LogingContext'
export default function Login(){
  
    const [IsLoging, setIsLoging] = useState(false);
 
    const [form, setForm]= useState ({
      nom:'', prenom:'',email:'', mdp:''
    });
    const handleChangenom = (event:any) => {
      setForm({
        ...form,
        nom:event.target.value
      });
      //console.log(form);
    };
    const handleChangePrenom = (event:any) => {
      setForm({
        ...form,
        prenom:event.target.value
      });
     // console.log(form);
    };
    const handleChangeEmail = (event:any) => {
      setForm({
        ...form,
        email:event.target.value
      });
    
    };
    const handleChangeMDP = (event:any) => {
      setForm({
        ...form,
        mdp:event.target.value
      });
   
    };
    async function test(){
      
      try {
        const response = await fetch('http://localhost:1337/api/auth/local/',
         {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            identifier:form.nom,
            password:form.mdp
            })
        });
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        const data = await response.json();
        console.log(data.jwt!== "")
     
        if (data.jwt!== ""){
          
          setIsLoging(true)
          console.log(IsLoging)
        }
    
        return IsLoging
      } catch (error) {
        console.error(error);
      }
    }
  
    async function verifier(){
      const response = await fetch('http://localhost:1337/api/Users');
      const liste = await response.json();
    test();
   // console.log(IsLoging)
   for (let i=0; i < liste.length; i++){
      if(liste[i].username=== form.nom && liste[i].email=== form.email&& IsLoging){
      //  console.log(`Bienvenu ${form.nom} ${form.prenom}` )
        // alert(`Bienvenu ${form.nom} ${form.prenom}`)
        return alert(`Bienvenu ${form.nom} ${form.prenom}`)
       // return document.getElementsByClassName("App").innterHTML += ` <h2>Bienvenu ${form.nom}${form.prenom}</h2>`
      }else return alert(`c'est faux`);
    }
    }
    return (
      <div className="App">
        <form onSubmit={verifier}>
        <div className="rounded-lg square border border-dark w-80 h-70 mt-10 ml-100">
                <div>
              <label>Nom<em>*</em> :</label><br/>
              <input  className="square border border-dark" type="text" id="nom" name="nom" onChange={handleChangenom}/>
          </div>
          <div>
              <label>prénom<em>*</em> :</label><br/>
              <input className="square border border-dark" type="text" id="prenom" name="prenom"onChange={handleChangePrenom}/>
          </div>
          <div>
              <label>e-mail<em>*</em> :</label><br/>
              <input className="square border border-dark"  type="email" id="email" name="email"onChange={handleChangeEmail}/>
          </div>
          <div>
              <label >mot de passe<em>*</em> :</label><br/>
              <input  className="square border border-dark" type="password" id="mdp" name="mdp"onChange={handleChangeMDP}/>
          </div>
         
     
        <Link to="/Todolist">
        <button className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 border border-green-700 rounded w-24"type="submit">Login</button>
      
        </Link>
        </div>
        </form>
      </div>
    );
}
//export default home;

