import { createContext, useState } from "react"
export const LogingContext =createContext();
export const ThemeContext = ({ children}) => {
    const [IsLoging, setIsLoging] = useState(false)
   /* const toggleTheme = () => {
        setIsLoging(IsLoging === false ? true : false)
    }*/
 
    return (
        <LogingContext.Provider value={{ IsLoging, setIsLoging }}>
            {children}
        </LogingContext.Provider>
    )
}