import './App.css';
import React from 'react';
import { Routes, Route } from 'react-router-dom';
 
import Login from './components/Login';

import Todolist from './components/todolist';
import Navbar from './components/Navbar/Navigation';
//import Contact from './components/Contact';
//import Error from './components/Error';

function Menu(){

  return ( 
    <div className='Menu'>
       <Navbar/> 
 <Routes>  
 <Route path="/" element={<Login />}/>
 <Route path="/todolist" element={<Todolist />}/>
 </Routes>


    </div>     
 );
}
 

export default Menu;
